
public class Main {
	public static void main(String[] args) throws FullException{
		Refrigerator r = new Refrigerator(5);
		
		try {
			// add element to arraylist Refridgerator
			r.put("Apple");
			r.put("Melon");
			r.put("Pork");
			r.put("Orange");
			r.put("Water");
			
			// arraylist Refridgerator
			System.out.println(" In Refridgerator ");
			System.out.println(r.toString());
			 // take elements take out from arraylist
			System.out.println(" Take out ");
			System.out.println(r.takeOut("Orange"));
			System.out.println(r.takeOut("pineapple"));
			System.out.println();
			 // show list after take out from arraylist
			System.out.println(" After take out ");
			System.out.println(r.toString());
			 // add element to arraylist Refridgerator
			System.out.println();
			System.out.println(" put in Refridgerator ");
			r.put("Egg");
			System.out.println(r.toString());
			 // add element to arraylist Refridgerator FULL!!
			System.out.println();
			System.out.println("put in Refridgerator ");
			r.put("Snack");
			   
			 
	 	 }
		catch (FullException e){
			   System.err.println("Error: " + e.getMessage());
			  } 
			
	
	}
}
