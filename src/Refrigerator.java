

import java.util.ArrayList;

public class Refrigerator {
	 private int size;
	 private ArrayList<String> things;
	 
	 public Refrigerator(int size){
		  things = new ArrayList<String>();
		  this.size = size;
		 }
	 
	 public void put(String stuff) throws FullException{
		  if (things.size() < size){
		   things.add(stuff);
		  }
		  else {
		   throw new FullException("Refridgerator is full");
		  }
		  
	 }
	 
	 public String takeOut(String stuff){
		  if (things.contains(stuff) == true){
		   String r = things.remove(things.indexOf(stuff));
		   return "take out : " + r;
		  }
		  else {
		   return stuff + " is " + null;
		  }  
	 }
	 
	 public String toString(){
		  String list = "";
		  for (String x : things){
		   list = list + "stuff in : "+ x + " " + "\n";
		  }
		  return list;
		 }
	}
